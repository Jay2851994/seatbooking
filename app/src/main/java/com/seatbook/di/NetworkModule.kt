package com.seatbook.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.seatbook.api.Api
import com.seatbook.app.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule() {

    @Provides
    fun provideAPIService(): Api {

        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        val gson = gsonBuilder.create()

        val httpClient = OkHttpClient.Builder()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val requestBuilder = original.newBuilder()
            requestBuilder.addHeader("Cache-Control", "no-cache")

            val request = requestBuilder.build()
            chain.proceed(request)

        }

        httpClient.interceptors().add(Interceptor { chain ->
            val request = chain.request()
            val response = chain.proceed(request)
            response.code()
            response
        })

        val okHttpClient = httpClient.addInterceptor(interceptor)
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .cache(null)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.API_END_POINT)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))

        return retrofit
            .build()
            .create(Api::class.java)

    }

}
