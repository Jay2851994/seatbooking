package com.seatbook.di

import com.seatbook.api.Api
import com.seatbook.repository.SeatRepository
import com.seatbook.utils.resource.ResponseHandler
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Module
@InstallIn(ApplicationComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideSeatRepository(
        api: Api,
        responseHandler: ResponseHandler
    ): SeatRepository {
        return SeatRepository(api, responseHandler)
    }

}