package com.seatbook.di

import android.content.Context
import android.content.SharedPreferences
import com.airlurn.app.utils.*
import com.seatbook.utils.ErrorUtils
import com.seatbook.utils.resource.ResponseHandler
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Module
@InstallIn(ApplicationComponent::class)
class UtilsModule() {

    @Singleton
    @Provides
    fun provideErrorUtils(@ApplicationContext context: Context): ErrorUtils {
        return ErrorUtils(context)
    }

    @Singleton
    @Provides
    fun provideResponseHandler(
        @ApplicationContext context: Context,
        errorUtils: ErrorUtils
    ): ResponseHandler {
        return ResponseHandler(context, errorUtils)
    }

}