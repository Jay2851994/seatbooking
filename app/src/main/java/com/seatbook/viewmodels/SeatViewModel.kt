package com.seatbook.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.seatbook.model.SeatInfo
import com.seatbook.model.Seats
import com.seatbook.model.SeatsAvailability
import com.seatbook.repository.SeatRepository
import com.seatbook.utils.resource.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class SeatViewModel
@ViewModelInject
constructor(
    private val repository: SeatRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val seatsAvailabilityAvNetworkState: MutableLiveData<Resource<SeatInfo>> =
        MutableLiveData()

    fun seatsAvailabilityAvNetworkState(): LiveData<Resource<SeatInfo>> {
        return seatsAvailabilityAvNetworkState
    }

    fun getSeatsAvailability() {

        viewModelScope.launch {

            repository.getSeatsAvailability()
                .onEach { state ->
                    seatsAvailabilityAvNetworkState.value = state
                }
                .launchIn(viewModelScope)
        }

    }

}