package com.seatbook.api

import com.seatbook.model.SeatsAvailability
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @GET("test-api")
    suspend fun getSeatsAvailability(): Response<SeatsAvailability>


}