package com.seatbook.model


data class Error(
    val errorCode: Int = 0,
    val errorMessage: String? = ""
)


