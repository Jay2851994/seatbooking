package com.seatbook.model


import com.google.gson.annotations.SerializedName

data class SeatsAvailability(
    @SerializedName("Status")
    val status: String = "",
    @SerializedName("Data")
    val data: Data,
    @SerializedName("Page")
    val page: Page,
    @SerializedName("StatusCode")
    val statusCode: Int = 0
)


data class ZonesItem(
    @SerializedName("startIndex")
    val startIndex: Int = 0,
    @SerializedName("maxRows")
    val maxRows: Int = 0,
    @SerializedName("price")
    val price: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("colorCode")
    val colorCode: String = "",
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("sort")
    val sort: Int = 0,
    @SerializedName("maxColumns")
    val maxColumns: Int = 0,
    @SerializedName("seats")
    val seats: List<SeatsItem>?,
    @SerializedName("totalSeats")
    val totalSeats: Int = 0,
    @SerializedName("seatsBooked")
    val seatsBooked: Int = 0
)


data class Data(
    @SerializedName("extras")
    val extras: List<ExtrasItem>?,
    @SerializedName("booking_status")
    val bookingStatus: Boolean = false,
    @SerializedName("zones")
    val zones: List<ZonesItem>?
)


data class Page(
    @SerializedName("limit")
    val limit: String = "",
    @SerializedName("totalCount")
    val totalCount: String = ""
)


data class ExtrasItem(
    @SerializedName("price")
    val price: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("id")
    val id: Int = 0
)


data class SeatsItem(
    @SerializedName("availableStatus")
    val availableStatus: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("seatKey")
    val seatKey: String = "",
    @SerializedName("columnIndex")
    val columnIndex: Int = 0,
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("rowNumber")
    val rowNumber: Int = 0,
    @SerializedName("bookedStatus")
    val bookedStatus: Int = 0
)


