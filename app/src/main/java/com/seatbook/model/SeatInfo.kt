package com.seatbook.model

data class SeatInfo(val maxColumns: Int, val seats: ArrayList<Seats>)