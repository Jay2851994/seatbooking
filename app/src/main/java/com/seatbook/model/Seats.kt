package com.seatbook.model

data class Seats(
    val seatName: String = "",
    val seatRaw: Int = -1,
    val seatColumn: Int = -1,
    val zoneName: String = "",
    val isEmpty: Boolean = false,
    var availabilityStatus : Int = 2,
    var isSelected : Boolean = false
)