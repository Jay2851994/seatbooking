package com.seatbook.utils

import android.content.Context
import com.airlurn.app.utils.exceptions.APIException
import com.airlurn.app.utils.exceptions.EmptyException
import com.airlurn.app.utils.exceptions.ValidationException
import com.seatbook.R
import com.seatbook.app.Constants
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import com.seatbook.model.Error

class ErrorUtils(private val context: Context) {

    fun handleError(e: Exception?): Error {

        return if (e != null) {

            if (e is APIException) {
                Error(
                    Constants.SERVER_ERROR,
                    e.localizedMessage
                )
            } else if (e is EmptyException) {
                Error(
                    Constants.EMPTY_ERROR,
                    context.getString(R.string.no_data)
                )
            } else if (e is ValidationException) {
                Error(
                    Constants.VALIDATION_ERROR,
                    e.message
                )
            } else if (e is IllegalStateException) {
                Error(
                    Constants.SERVER_ERROR,
                    e.localizedMessage
                )
            } else if (e is SocketTimeoutException) {
                Error(
                    Constants.SERVER_ERROR,
                    context.getString(R.string.time_out)
                )
            } else if (e is ConnectException || e is UnknownHostException) {
                Error(
                    Constants.INTERNET_ERROR,
                    context.getString(R.string.no_internet)
                )
            } else {
                Error(
                    Constants.UNKNOWN_ERROR,
                    if (e.message != null) e.message else context.getString(R.string.unknown_error)
                )
            }

        } else {
            Error(
                Constants.UNKNOWN_ERROR,
                context.getString(R.string.unknown_error)
            )
        }

    }

}