package com.seatbook.utils.resource

import android.content.Context
import com.airlurn.app.utils.exceptions.EmptyResponseBodyException
import com.seatbook.utils.ErrorUtils
import retrofit2.Response


class ResponseHandler(
    val context: Context,
    val errorUtils: ErrorUtils
) {

    fun <T : Any> handleResponse(response: Response<T>): Resource<T> {

        if (response.isSuccessful) {

            val responseBody = response.body()

            return if (responseBody != null) {
                Resource.success(responseBody)
            } else {
                Resource.error(errorUtils.handleError(EmptyResponseBodyException()))
            }

        } else {

            return Resource.error(errorUtils.handleError(null))

        }

    }

    fun <T : Any> handleCustomException(e: Exception?): Resource<T> {
        return Resource.error(errorUtils.handleError(e))
    }

    fun <T : Any> handleException(e: Exception?): Resource<T> {
        return Resource.error(errorUtils.handleError(e))
    }

}
