package com.seatbook.utils.resource

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}