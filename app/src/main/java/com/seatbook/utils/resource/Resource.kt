package com.seatbook.utils.resource
import com.seatbook.model.Error


data class Resource<out T>(
    val status: Status,
    val data: T?,
    val error: Error? = null
) {

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(error: Error): Resource<T> {
            return Resource(Status.ERROR, null,  error)
        }

        fun <T> loading(): Resource<T> {
            return Resource(
                Status.LOADING,
                null,
                null
            )
        }

    }

}