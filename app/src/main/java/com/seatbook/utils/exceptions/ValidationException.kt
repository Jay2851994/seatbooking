package com.airlurn.app.utils.exceptions

class ValidationException : Exception {
    constructor(message: String?) : super(message)
    constructor(message: String?, throwable: Throwable?) : super(message, throwable)
}