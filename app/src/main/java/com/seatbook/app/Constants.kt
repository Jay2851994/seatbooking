package com.seatbook.app

class Constants {

    companion object {

        const val BASE_URL: String = "http://rt.dev.mobiiworld.com"

        const val API_END_POINT: String = "$BASE_URL/api/"

        val EMPTY_ERROR: Int = 1001
        val INTERNET_ERROR: Int = 1002
        val SERVER_ERROR: Int = 1003
        val TOKEN_EXPIRED_ERROR: Int = 1004
        val VALIDATION_ERROR: Int = 1005
        val UNKNOWN_ERROR: Int = 5001

        val SUCCESS: String = "Success"

    }
}