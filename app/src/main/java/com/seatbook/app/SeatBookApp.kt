package com.seatbook.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SeatBookApp : Application() {

    override fun onCreate() {
        super.onCreate()
    }

}