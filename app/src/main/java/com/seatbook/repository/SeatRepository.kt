package com.seatbook.repository

import com.seatbook.api.Api
import com.seatbook.app.Constants
import com.seatbook.model.SeatInfo
import com.seatbook.model.Seats
import com.seatbook.model.SeatsAvailability
import com.seatbook.utils.resource.Resource
import com.seatbook.utils.resource.ResponseHandler
import com.seatbook.utils.resource.Status
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SeatRepository constructor(
    private val api: Api,
    private val responseHandler: ResponseHandler
) {

    suspend fun getSeatsAvailability(): Flow<Resource<SeatInfo>> = flow {

        emit(Resource.loading())

        try {

            val response = responseHandler.handleResponse(api.getSeatsAvailability())

            val responseBody = response.data

            if (responseBody != null) {

                if (responseBody.status == Constants.SUCCESS) {

                    val seatsList = ArrayList<Seats>()

                    val maxColumns = ArrayList<Int>()

                    var zones = responseBody.data.zones

                    if (!zones.isNullOrEmpty()) {

                        zones = zones.sortedBy { it.startIndex }

                        for (z in 0 until zones.size) {

                            maxColumns.add(zones[z].maxColumns)

                            for (i in 0 until zones[z].maxRows) {

                                for (j in 0 until zones[z].maxColumns) {

                                    val seats = zones[z].seats

                                    if (!seats.isNullOrEmpty()) {

                                        var isEmpty = true

                                        var maxRows = 0

                                        for (zone in 0 until z) {
                                            maxRows += zones[zone].maxRows
                                        }

                                        val index = zones[z].startIndex + maxRows

                                        for (s in 0 until seats.size) {

                                            if (seats[s].rowNumber == i + index && seats[s].columnIndex == j + 1) {
                                                isEmpty = false
                                                seatsList.add(
                                                    Seats(
                                                        seatName = seats[s].name,
                                                        seatRaw = seats[s].rowNumber,
                                                        seatColumn = seats[s].columnIndex,
                                                        isEmpty = false,
                                                        availabilityStatus = seats[s].bookedStatus,
                                                        zoneName = zones[z].name
                                                    )
                                                )
                                                break
                                            } else {
                                                isEmpty = true
                                            }

                                        }

                                        if (isEmpty)
                                            seatsList.add(
                                                Seats(
                                                    isEmpty = true
                                                )
                                            )

                                    }

                                }

                            }

                        }

                    }

                    emit(Resource.success(SeatInfo(maxColumns = maxColumns.max()!!, seats = seatsList)))

                } else {
                    emit(responseHandler.handleException<SeatInfo>(null))
                }

            }

        } catch (e: Exception) {
            emit(responseHandler.handleException<SeatInfo>(e))
        }

    }


}