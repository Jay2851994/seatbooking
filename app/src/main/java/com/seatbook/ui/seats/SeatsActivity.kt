package com.seatbook.ui.seats

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.seatbook.R
import com.seatbook.app.BaseActivity
import com.seatbook.databinding.ActivitySeatsBinding
import com.seatbook.ui.seats.adapter.SeatsAdapter
import com.seatbook.utils.resource.Status
import com.seatbook.viewmodels.SeatViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SeatsActivity : BaseActivity(), SeatsAdapter.OnItemClickListener {

    private val viewModel: SeatViewModel by viewModels()

    private lateinit var binding: ActivitySeatsBinding

    private val selectedSeats = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySeatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {

        subscribeObservers()

        viewModel.getSeatsAvailability()

    }

    private fun subscribeObservers() {

        viewModel.seatsAvailabilityAvNetworkState()
            .observe(this, { networkState ->

                when (networkState.status) {

                    Status.LOADING -> {

                    }

                    Status.SUCCESS -> {

                        val beans = networkState.data?.seats

                        if (!beans.isNullOrEmpty()) {

                            val layoutManager = GridLayoutManager(this, networkState?.data.maxColumns)

                            binding.recyclerView.layoutManager = layoutManager

                            val adapter = SeatsAdapter(this, this@SeatsActivity)
                            binding.recyclerView.adapter = adapter
                            adapter.addItems(beans)

                        }

                    }

                    Status.ERROR -> {


                        val error = networkState?.error

                        if (error != null) {

                        }

                    }

                }
            })

    }

    override fun onItemClicked(seatName: String, isAdded: Boolean) {

        if (isAdded)
            selectedSeats.add(seatName)
        else
            selectedSeats.remove(seatName)

        if (selectedSeats.size > 0) {
            binding.txtSelectedSeats.visibility = View.VISIBLE
            binding.txtSelectedSeats.text = getString(R.string.selected_seats,TextUtils.join(",", selectedSeats))
        } else {
            binding.txtSelectedSeats.visibility = View.INVISIBLE
        }

    }

    override fun onZon0LimitExceeded() {
        Toast.makeText(this,"You can only select up to 3 seats in zone 0!",Toast.LENGTH_SHORT).show()
    }

    override fun onOtherZoneLimitExceeded() {
        Toast.makeText(this,"You can only select up to 4 seats!",Toast.LENGTH_SHORT).show()
    }

}