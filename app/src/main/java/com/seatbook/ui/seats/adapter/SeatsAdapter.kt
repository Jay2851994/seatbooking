package com.seatbook.ui.seats.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seatbook.R
import com.seatbook.databinding.RawEmptyBinding
import com.seatbook.databinding.RawSeatsBinding
import com.seatbook.model.Seats

class SeatsAdapter(
    private val context: Context,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClicked(seatName: String, isAdded: Boolean)
        fun onZon0LimitExceeded()
        fun onOtherZoneLimitExceeded()
    }

    private val VIEW_ITEM = 1
    private val VIEW_EMPTY = 2

    private var beans = ArrayList<Seats>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {

        if (viewType == VIEW_EMPTY) {

            return EmptyViewHolder(
                RawEmptyBinding.inflate(
                    LayoutInflater.from(context),
                    parent,
                    false
                )
            )

        } else {

            return ItemViewHolder(
                RawSeatsBinding.inflate(
                    LayoutInflater.from(context),
                    parent,
                    false
                )
            )

        }

    }

    override fun getItemViewType(position: Int): Int {
        if (beans[position].isEmpty)
            return VIEW_EMPTY
        return VIEW_ITEM
    }

    override fun getItemCount(): Int {
        return beans.size
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {

        val bean = beans[position]

        if (holder is ItemViewHolder) {

            if (bean.availabilityStatus == 0) {
                holder.img.setImageResource(R.drawable.ic_seat_avail)
            } else if (bean.availabilityStatus == 1) {
                holder.img.setImageResource(R.drawable.ic_seat_booked)
            } else {
                holder.img.setImageResource(R.drawable.ic_seat_unavail)
            }

            holder.itemView.setOnClickListener {

                if (bean.availabilityStatus != 1) {


                    if (bean.availabilityStatus == 0) {

                        if (bean.zoneName == "zone0") {

                            if (isZone0LimitExceeded()) {

                                onItemClickListener.onZon0LimitExceeded()

                            } else {

                                bean.availabilityStatus = 2
                                bean.isSelected = true
                                holder.img.setImageResource(R.drawable.ic_seat_unavail)

                            }

                        } else {

                            if (isOtherZoneLimitExceeded()) {

                                onItemClickListener.onOtherZoneLimitExceeded()

                            } else {

                                bean.availabilityStatus = 2
                                bean.isSelected = true
                                holder.img.setImageResource(R.drawable.ic_seat_unavail)

                            }

                        }

                    } else if (bean.availabilityStatus == 2) {

                        bean.availabilityStatus = 0
                        bean.isSelected = false
                        holder.img.setImageResource(R.drawable.ic_seat_avail)

                    }

                    onItemClickListener.onItemClicked(
                        bean.seatName,
                        bean.availabilityStatus == 2
                    )

                }

            }

        }

    }


    fun isZone0LimitExceeded(): Boolean {

        var count = 0

        for (bean in beans) {

            if (bean.isSelected) {
                if (bean.zoneName == "zone0") {
                    count += 1
                }
            }

        }

        return count == 3

    }

    fun isOtherZoneLimitExceeded(): Boolean {

        var count = 0

        for (bean in beans) {

            if (bean.isSelected) {
                if (bean.zoneName != "zone0") {
                    count += 1
                }
            }

        }

        return count == 4

    }

    fun addItems(result: List<Seats>?) {
        beans.addAll(result!!)
        notifyDataSetChanged()
    }


    class ItemViewHolder(binding: RawSeatsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val img = binding.img
    }

    class EmptyViewHolder(binding: RawEmptyBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }


}